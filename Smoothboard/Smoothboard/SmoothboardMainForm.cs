﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Windows.Forms;
using Newtonsoft.Json;
using Smoothboard.Resources;

namespace Smoothboard {
    public partial class SmoothboardMainForm : Form {

        public Size OriginalImageSize;
        public Size ModifiedImageSize;

        public static Dossier Dossier = new Dossier();
        public static Image Design;
        public static string DesignPath;
        public static Image surfboard = Res.surfboard_outline;
        public SmoothboardMainForm() {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e) {
            if (String.IsNullOrEmpty(tbAddress.Text) ||
                String.IsNullOrEmpty(tbBrought.Text) ||
                String.IsNullOrEmpty(tbEmail.Text) ||
                String.IsNullOrEmpty(tbName.Text) ||
                String.IsNullOrEmpty(tbNumber.Text) ||
                String.IsNullOrEmpty(tbTake.Text) ||
                String.IsNullOrEmpty(lbCustomDesign.Text)) {

                MessageBox.Show("Een of meer velden zijn niet ingevuld", "Veld(en) leeg", MessageBoxButtons.OK);
            }
            else {
                if (!IsValidEmail(tbEmail.Text)) {
                    MessageBox.Show("Geen geldig e-mail adres", "Ongeldige email");
                    return;
                }

                int parseResult;
                if (int.TryParse(tbNumber.Text, out parseResult)) {
                    Console.WriteLine("Valid integer: " + parseResult);
                }
                else {
                    Console.WriteLine("Not a valid integer");
                }

                Dossier.Address = tbAddress.Text;
                Dossier.BroughtAt = tbBrought.Text;
                Dossier.Comment = tbComment.Text;
                Dossier.Email = tbEmail.Text;
                Dossier.Name = tbName.Text;
                Dossier.Number = tbNumber.Text;
                Dossier.Size = cbSize.Text;
                Dossier.TakeAt = tbTake.Text;
                Dossier.Design = DesignPath;

                string json = JsonConvert.SerializeObject(Dossier);
                File.WriteAllText(String.Format(@"{0}\Dossiers\{1}.smtb", Application.LocalUserAppDataPath, Dossier.Name), json);

                DialogResult result = MessageBox.Show("Wilt u het design naar de opdrachtgever mailen?", "Design mailen?", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes) {
                    SendMail();
                }
                tbcMain.SelectTab(0);

                tbAddress.Text = "";
                tbBrought.Text = "";
                tbComment.Text = "";
                tbEmail.Text = "";
                tbNumber.Text = "";
                cbSize.Text = "";
                tbTake.Text = "";
                DesignPath = "";
                lbCustomDesign.Text = "...";
            }
        }

        public bool IsValidEmail(string email) {
            try {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch {
                return false;
            }
        }
        private void SendMail() {
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Port = Properties.Settings.Default.SmtpPort;
            smtpClient.Host = Properties.Settings.Default.SmtpHost;

            MailMessage message = new MailMessage();
            Attachment attachment = new Attachment(Dossier.Design);
            message.Attachments.Add(attachment);
            message.From = new MailAddress("Service@smtb.com");
            message.To.Add(Dossier.Email);
            message.Subject = "Uw Surfboard Design";
            message.Body = String.Format("Beste {0}, Ik heb een ontwerp gemaakt voor uw surfboard, ik zou graag willen weten of u tevreden bent met dit ontwerp. Dit ontwerp staat in de bijlage.", Dossier.Name);

            try {
                smtpClient.Send(message);
            }
            catch (Exception ex) {
                MessageBox.Show(String.Format("Mail sturen mislukt. \n\n {0}", ex.ToString()));
            }
        }

        private void tbcMain_SelectedIndexChanged(object sender, EventArgs e) {
            if (tbcMain.SelectedIndex == 2) {
                tbBrought.Text = DateTime.Today.Date.ToString("d");
            }
            switch (tbcMain.SelectedIndex) {
                case 0:

                    lbDossiers.Items.Clear();
                    DirectoryInfo AppDataInfo = new DirectoryInfo(String.Format(@"{0}\Dossiers", Application.LocalUserAppDataPath));

                    FileInfo[] Files = AppDataInfo.GetFiles();

                    foreach (FileInfo Dossier in Files) {
                        lbDossiers.Items.Add(Dossier.Name);
                    }

                    break;
                case 1:
                    tbBrought.Text = DateTime.Today.Date.ToString("d");
                    break;
                case 2:
                    tbSmtpPort.Text = Properties.Settings.Default.SmtpPort.ToString();
                    tbSmtpHost.Text = Properties.Settings.Default.SmtpHost;
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void btnGenerateDesign_Click(object sender, EventArgs e) {

                OpenFileDialog dialog = new OpenFileDialog();
                DialogResult result = dialog.ShowDialog();
                if (DialogResult.OK == result) {
                    Design = Image.FromFile(dialog.FileName);
                    lbCustomDesign.Text = dialog.FileName;
            }

            using (Bitmap bitmap = new Bitmap(250, 647)) {
                using (Graphics canvas = Graphics.FromImage(bitmap)) {
                    canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    canvas.DrawImage(Design, 0, 0);
                    canvas.DrawImage(surfboard, new Rectangle(0, 0, 250, 647));
                    canvas.DrawString(DateTime.Today.Date.ToString("yy-mm-dd"), DefaultFont, Brushes.Black, 5, 5);
                    canvas.Save();

                    //try {
                    DesignPath = String.Format(@"{0}\Designs\{1}_Design-{2}.bmp", Application.LocalUserAppDataPath, tbName.Text, DateTime.Today.Date.ToString("yy-mm-dd"));
                    bitmap.Save(DesignPath);

                    MessageBox.Show(String.Format("Uw design is opgeslagen in {0}", DesignPath));
                    //}
                    //catch (Exception ex) {
                    //    MessageBox.Show(ex.ToString());
                    //}
                }
            }
        }

        private void btnCustomDesign_Click(object sender, EventArgs e) {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();
            if (DialogResult.OK == result) {
                Design = Image.FromFile(dialog.FileName);
                lbCustomDesign.Text = dialog.FileName;
            }
        }

        private void lbDossiers_SelectedIndexChanged(object sender, EventArgs e) {
            gbInfo.Enabled = true;
            //FileInfo fileInfo = new FileInfo(String.Format(@"{0}\{1}",Application.LocalUserAppDataPath,lbDossiers.SelectedItem));

            string jsonstring = File.ReadAllText(String.Format(@"{0}\Dossiers\{1}", Application.LocalUserAppDataPath, lbDossiers.SelectedItem));
            Dossier json = JsonConvert.DeserializeObject<Dossier>(jsonstring);

            lbName.Text = json.Name;
            lbAddress.Text = json.Address;
            lbNumber.Text = json.Number;
            lbEmail.Text = json.Email;
            lbSize.Text = json.Size;
            lbBrought.Text = json.BroughtAt;
            lbTake.Text = json.TakeAt;
            tbDocComment.Text = json.Comment;

            Image preview = Image.FromFile(json.Design);
            pbPreview.Image = preview;

            Dossier = json;
        }

        private void btnMail_Click(object sender, EventArgs e) {
            SendMail();
        }

        private void SmoothboardMainForm_Load(object sender, EventArgs e) {
            DirectoryInfo AppDataInfo = new DirectoryInfo(String.Format(@"{0}\Dossiers", Application.LocalUserAppDataPath));

            FileInfo[] Files = AppDataInfo.GetFiles();

            foreach (FileInfo Dossier in Files) {
                lbDossiers.Items.Add(Dossier.Name);
            }
        }

        private void tbUpdateSettings_Click(object sender, EventArgs e) {
            Properties.Settings.Default.SmtpHost = tbSmtpHost.Text;
            Properties.Settings.Default.SmtpPort = Convert.ToInt32(tbSmtpPort.Text);
        }

        private void OpenDossiers_Click(object sender, EventArgs e) {
            Process.Start(String.Format(@"{0}\Dossiers", Application.LocalUserAppDataPath));
        }

        private void OpenDesigns_Click(object sender, EventArgs e) {
            Process.Start(String.Format(@"{0}\Designs", Application.LocalUserAppDataPath));
        }
    }
}