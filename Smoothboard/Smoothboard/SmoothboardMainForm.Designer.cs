﻿namespace Smoothboard {
    partial class SmoothboardMainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SmoothboardMainForm));
            this.tpNewDossier = new System.Windows.Forms.TabPage();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbComment = new System.Windows.Forms.GroupBox();
            this.tbComment = new System.Windows.Forms.TextBox();
            this.gbDesign = new System.Windows.Forms.GroupBox();
            this.lbCustomDesign = new System.Windows.Forms.Label();
            this.btnGenerateDesign = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.gbCustomer = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbSize = new System.Windows.Forms.ComboBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbTake = new System.Windows.Forms.TextBox();
            this.tbBrought = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tpDossiers = new System.Windows.Forms.TabPage();
            this.gbInfo = new System.Windows.Forms.GroupBox();
            this.tbDocComment = new System.Windows.Forms.TextBox();
            this.btnMail = new System.Windows.Forms.Button();
            this.pbPreview = new System.Windows.Forms.PictureBox();
            this.lbEmail = new System.Windows.Forms.Label();
            this.lbTake = new System.Windows.Forms.Label();
            this.lbBrought = new System.Windows.Forms.Label();
            this.lbNumber = new System.Windows.Forms.Label();
            this.lbSize = new System.Windows.Forms.Label();
            this.lbAddress = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbDossiers = new System.Windows.Forms.ListBox();
            this.tbcMain = new System.Windows.Forms.TabControl();
            this.tpSettings = new System.Windows.Forms.TabPage();
            this.tbUpdateSettings = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tbSmtpHost = new System.Windows.Forms.TextBox();
            this.tbSmtpPort = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.OpenDossiers = new System.Windows.Forms.Button();
            this.OpenDesigns = new System.Windows.Forms.Button();
            this.tpNewDossier.SuspendLayout();
            this.gbComment.SuspendLayout();
            this.gbDesign.SuspendLayout();
            this.gbCustomer.SuspendLayout();
            this.tpDossiers.SuspendLayout();
            this.gbInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tbcMain.SuspendLayout();
            this.tpSettings.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpNewDossier
            // 
            this.tpNewDossier.Controls.Add(this.btnSave);
            this.tpNewDossier.Controls.Add(this.gbComment);
            this.tpNewDossier.Controls.Add(this.gbDesign);
            this.tpNewDossier.Controls.Add(this.gbCustomer);
            this.tpNewDossier.Location = new System.Drawing.Point(4, 34);
            this.tpNewDossier.Name = "tpNewDossier";
            this.tpNewDossier.Padding = new System.Windows.Forms.Padding(3);
            this.tpNewDossier.Size = new System.Drawing.Size(631, 327);
            this.tpNewDossier.TabIndex = 2;
            this.tpNewDossier.Text = "Nieuw Dossier";
            this.tpNewDossier.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.btnSave.Location = new System.Drawing.Point(12, 272);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(232, 40);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Dossier Opslaan";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbComment
            // 
            this.gbComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbComment.Controls.Add(this.tbComment);
            this.gbComment.Location = new System.Drawing.Point(250, 80);
            this.gbComment.Name = "gbComment";
            this.gbComment.Size = new System.Drawing.Size(366, 232);
            this.gbComment.TabIndex = 8;
            this.gbComment.TabStop = false;
            this.gbComment.Text = "Extra Commentaar / Wensen";
            // 
            // tbComment
            // 
            this.tbComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbComment.Location = new System.Drawing.Point(6, 23);
            this.tbComment.Multiline = true;
            this.tbComment.Name = "tbComment";
            this.tbComment.Size = new System.Drawing.Size(354, 198);
            this.tbComment.TabIndex = 8;
            // 
            // gbDesign
            // 
            this.gbDesign.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDesign.Controls.Add(this.lbCustomDesign);
            this.gbDesign.Controls.Add(this.btnGenerateDesign);
            this.gbDesign.Controls.Add(this.label14);
            this.gbDesign.Controls.Add(this.label15);
            this.gbDesign.Location = new System.Drawing.Point(250, 6);
            this.gbDesign.Name = "gbDesign";
            this.gbDesign.Size = new System.Drawing.Size(366, 68);
            this.gbDesign.TabIndex = 6;
            this.gbDesign.TabStop = false;
            this.gbDesign.Text = "Design";
            // 
            // lbCustomDesign
            // 
            this.lbCustomDesign.AutoSize = true;
            this.lbCustomDesign.Location = new System.Drawing.Point(80, 48);
            this.lbCustomDesign.Name = "lbCustomDesign";
            this.lbCustomDesign.Size = new System.Drawing.Size(16, 13);
            this.lbCustomDesign.TabIndex = 6;
            this.lbCustomDesign.Text = "...";
            // 
            // btnGenerateDesign
            // 
            this.btnGenerateDesign.BackColor = System.Drawing.Color.White;
            this.btnGenerateDesign.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGenerateDesign.BackgroundImage")));
            this.btnGenerateDesign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnGenerateDesign.Location = new System.Drawing.Point(6, 18);
            this.btnGenerateDesign.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnGenerateDesign.Name = "btnGenerateDesign";
            this.btnGenerateDesign.Size = new System.Drawing.Size(22, 21);
            this.btnGenerateDesign.TabIndex = 5;
            this.btnGenerateDesign.UseVisualStyleBackColor = false;
            this.btnGenerateDesign.Click += new System.EventHandler(this.btnGenerateDesign_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(34, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(247, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Genereer Ontwerpvoorbeeld met ingevoerd Design";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(34, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Design";
            // 
            // gbCustomer
            // 
            this.gbCustomer.Controls.Add(this.label2);
            this.gbCustomer.Controls.Add(this.label4);
            this.gbCustomer.Controls.Add(this.cbSize);
            this.gbCustomer.Controls.Add(this.tbName);
            this.gbCustomer.Controls.Add(this.label12);
            this.gbCustomer.Controls.Add(this.label11);
            this.gbCustomer.Controls.Add(this.label6);
            this.gbCustomer.Controls.Add(this.label5);
            this.gbCustomer.Controls.Add(this.tbTake);
            this.gbCustomer.Controls.Add(this.tbBrought);
            this.gbCustomer.Controls.Add(this.tbEmail);
            this.gbCustomer.Controls.Add(this.tbAddress);
            this.gbCustomer.Controls.Add(this.tbNumber);
            this.gbCustomer.Controls.Add(this.label7);
            this.gbCustomer.Location = new System.Drawing.Point(12, 6);
            this.gbCustomer.Name = "gbCustomer";
            this.gbCustomer.Size = new System.Drawing.Size(232, 260);
            this.gbCustomer.TabIndex = 4;
            this.gbCustomer.TabStop = false;
            this.gbCustomer.Text = "Opdrachtgever";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(111, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Naam Opdrachtgever";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(111, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Adres";
            // 
            // cbSize
            // 
            this.cbSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSize.FormattingEnabled = true;
            this.cbSize.Items.AddRange(new object[] {
            "260 x 65",
            "270 x 65",
            "280 x 65",
            "290 x 70",
            "300 x 70",
            "310 x 70",
            "320 x 75",
            "330 x 75",
            "340 x 80",
            "350 x 80",
            "360 x 85",
            "370 x 85"});
            this.cbSize.Location = new System.Drawing.Point(6, 136);
            this.cbSize.Name = "cbSize";
            this.cbSize.Size = new System.Drawing.Size(100, 21);
            this.cbSize.TabIndex = 5;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(6, 19);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(111, 237);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(81, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Datum Ophalen";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(111, 211);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Datum Gebracht";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(111, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "E-mail Adres";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(111, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Telefoon Nummer";
            // 
            // tbTake
            // 
            this.tbTake.Location = new System.Drawing.Point(6, 234);
            this.tbTake.Name = "tbTake";
            this.tbTake.Size = new System.Drawing.Size(100, 20);
            this.tbTake.TabIndex = 7;
            // 
            // tbBrought
            // 
            this.tbBrought.Location = new System.Drawing.Point(6, 208);
            this.tbBrought.Name = "tbBrought";
            this.tbBrought.Size = new System.Drawing.Size(100, 20);
            this.tbBrought.TabIndex = 6;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(6, 97);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(100, 20);
            this.tbEmail.TabIndex = 4;
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(6, 45);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(100, 20);
            this.tbAddress.TabIndex = 2;
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(6, 71);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(100, 20);
            this.tbNumber.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(111, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Maat Surfboard (in cm)";
            // 
            // tpDossiers
            // 
            this.tpDossiers.Controls.Add(this.gbInfo);
            this.tpDossiers.Controls.Add(this.groupBox1);
            this.tpDossiers.Location = new System.Drawing.Point(4, 34);
            this.tpDossiers.Name = "tpDossiers";
            this.tpDossiers.Padding = new System.Windows.Forms.Padding(3);
            this.tpDossiers.Size = new System.Drawing.Size(631, 327);
            this.tpDossiers.TabIndex = 1;
            this.tpDossiers.Text = "Dossiers";
            this.tpDossiers.UseVisualStyleBackColor = true;
            // 
            // gbInfo
            // 
            this.gbInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbInfo.Controls.Add(this.tbDocComment);
            this.gbInfo.Controls.Add(this.btnMail);
            this.gbInfo.Controls.Add(this.pbPreview);
            this.gbInfo.Controls.Add(this.lbEmail);
            this.gbInfo.Controls.Add(this.lbTake);
            this.gbInfo.Controls.Add(this.lbBrought);
            this.gbInfo.Controls.Add(this.lbNumber);
            this.gbInfo.Controls.Add(this.lbSize);
            this.gbInfo.Controls.Add(this.lbAddress);
            this.gbInfo.Controls.Add(this.lbName);
            this.gbInfo.Controls.Add(this.label8);
            this.gbInfo.Controls.Add(this.label18);
            this.gbInfo.Controls.Add(this.label17);
            this.gbInfo.Controls.Add(this.label9);
            this.gbInfo.Controls.Add(this.label16);
            this.gbInfo.Controls.Add(this.label1);
            this.gbInfo.Controls.Add(this.label10);
            this.gbInfo.Controls.Add(this.label13);
            this.gbInfo.Enabled = false;
            this.gbInfo.Location = new System.Drawing.Point(161, 6);
            this.gbInfo.Name = "gbInfo";
            this.gbInfo.Size = new System.Drawing.Size(455, 306);
            this.gbInfo.TabIndex = 8;
            this.gbInfo.TabStop = false;
            this.gbInfo.Text = "Dossier";
            // 
            // tbDocComment
            // 
            this.tbDocComment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDocComment.Enabled = false;
            this.tbDocComment.Location = new System.Drawing.Point(9, 232);
            this.tbDocComment.Multiline = true;
            this.tbDocComment.Name = "tbDocComment";
            this.tbDocComment.Size = new System.Drawing.Size(312, 64);
            this.tbDocComment.TabIndex = 11;
            // 
            // btnMail
            // 
            this.btnMail.BackColor = System.Drawing.Color.White;
            this.btnMail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMail.BackgroundImage")));
            this.btnMail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnMail.Location = new System.Drawing.Point(74, 101);
            this.btnMail.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnMail.Name = "btnMail";
            this.btnMail.Size = new System.Drawing.Size(22, 21);
            this.btnMail.TabIndex = 10;
            this.btnMail.UseVisualStyleBackColor = false;
            this.btnMail.Click += new System.EventHandler(this.btnMail_Click);
            // 
            // pbPreview
            // 
            this.pbPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbPreview.Location = new System.Drawing.Point(327, 19);
            this.pbPreview.Name = "pbPreview";
            this.pbPreview.Size = new System.Drawing.Size(122, 279);
            this.pbPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPreview.TabIndex = 9;
            this.pbPreview.TabStop = false;
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Location = new System.Drawing.Point(155, 105);
            this.lbEmail.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(16, 13);
            this.lbEmail.TabIndex = 1;
            this.lbEmail.Text = "...";
            // 
            // lbTake
            // 
            this.lbTake.AutoSize = true;
            this.lbTake.Location = new System.Drawing.Point(155, 189);
            this.lbTake.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.lbTake.Name = "lbTake";
            this.lbTake.Size = new System.Drawing.Size(16, 13);
            this.lbTake.TabIndex = 1;
            this.lbTake.Text = "...";
            // 
            // lbBrought
            // 
            this.lbBrought.AutoSize = true;
            this.lbBrought.Location = new System.Drawing.Point(155, 161);
            this.lbBrought.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.lbBrought.Name = "lbBrought";
            this.lbBrought.Size = new System.Drawing.Size(16, 13);
            this.lbBrought.TabIndex = 1;
            this.lbBrought.Text = "...";
            // 
            // lbNumber
            // 
            this.lbNumber.AutoSize = true;
            this.lbNumber.Location = new System.Drawing.Point(155, 77);
            this.lbNumber.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.lbNumber.Name = "lbNumber";
            this.lbNumber.Size = new System.Drawing.Size(16, 13);
            this.lbNumber.TabIndex = 1;
            this.lbNumber.Text = "...";
            // 
            // lbSize
            // 
            this.lbSize.AutoSize = true;
            this.lbSize.Location = new System.Drawing.Point(155, 133);
            this.lbSize.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.lbSize.Name = "lbSize";
            this.lbSize.Size = new System.Drawing.Size(16, 13);
            this.lbSize.TabIndex = 1;
            this.lbSize.Text = "...";
            // 
            // lbAddress
            // 
            this.lbAddress.AutoSize = true;
            this.lbAddress.Location = new System.Drawing.Point(155, 49);
            this.lbAddress.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(16, 13);
            this.lbAddress.TabIndex = 1;
            this.lbAddress.Text = "...";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(155, 21);
            this.lbName.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(16, 13);
            this.lbName.TabIndex = 1;
            this.lbName.Text = "...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 21);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Naam Opdrachtgever";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 133);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Maat Surfboard (in cm)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 77);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Telefoon Nummer";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 49);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Adres";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 105);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "E-mail Adres";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 217);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Extra Commentaar / Wensen";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 189);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Datum Ophalen";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 161);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Datum Gebracht";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.lbDossiers);
            this.groupBox1.Location = new System.Drawing.Point(12, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(143, 306);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dossiers";
            // 
            // lbDossiers
            // 
            this.lbDossiers.FormattingEnabled = true;
            this.lbDossiers.Location = new System.Drawing.Point(6, 19);
            this.lbDossiers.Name = "lbDossiers";
            this.lbDossiers.Size = new System.Drawing.Size(131, 277);
            this.lbDossiers.TabIndex = 0;
            this.lbDossiers.SelectedIndexChanged += new System.EventHandler(this.lbDossiers_SelectedIndexChanged);
            // 
            // tbcMain
            // 
            this.tbcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcMain.CausesValidation = false;
            this.tbcMain.Controls.Add(this.tpDossiers);
            this.tbcMain.Controls.Add(this.tpNewDossier);
            this.tbcMain.Controls.Add(this.tpSettings);
            this.tbcMain.ItemSize = new System.Drawing.Size(60, 30);
            this.tbcMain.Location = new System.Drawing.Point(-4, -3);
            this.tbcMain.Multiline = true;
            this.tbcMain.Name = "tbcMain";
            this.tbcMain.SelectedIndex = 0;
            this.tbcMain.Size = new System.Drawing.Size(639, 365);
            this.tbcMain.TabIndex = 0;
            this.tbcMain.SelectedIndexChanged += new System.EventHandler(this.tbcMain_SelectedIndexChanged);
            // 
            // tpSettings
            // 
            this.tpSettings.Controls.Add(this.groupBox2);
            this.tpSettings.Controls.Add(this.groupBox3);
            this.tpSettings.Location = new System.Drawing.Point(4, 34);
            this.tpSettings.Name = "tpSettings";
            this.tpSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tpSettings.Size = new System.Drawing.Size(631, 327);
            this.tpSettings.TabIndex = 3;
            this.tpSettings.Text = "Instellingen";
            this.tpSettings.UseVisualStyleBackColor = true;
            // 
            // tbUpdateSettings
            // 
            this.tbUpdateSettings.Location = new System.Drawing.Point(6, 71);
            this.tbUpdateSettings.Name = "tbUpdateSettings";
            this.tbUpdateSettings.Size = new System.Drawing.Size(123, 23);
            this.tbUpdateSettings.TabIndex = 6;
            this.tbUpdateSettings.Text = "Opslaan";
            this.tbUpdateSettings.UseVisualStyleBackColor = true;
            this.tbUpdateSettings.Click += new System.EventHandler(this.tbUpdateSettings_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.tbUpdateSettings);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.tbSmtpHost);
            this.groupBox3.Controls.Add(this.tbSmtpPort);
            this.groupBox3.Location = new System.Drawing.Point(12, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(208, 100);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "SMTP instellingen";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(135, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "SMTP Host";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(132, 48);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "SMTP Poort";
            // 
            // tbSmtpHost
            // 
            this.tbSmtpHost.Location = new System.Drawing.Point(6, 19);
            this.tbSmtpHost.Name = "tbSmtpHost";
            this.tbSmtpHost.Size = new System.Drawing.Size(123, 20);
            this.tbSmtpHost.TabIndex = 1;
            // 
            // tbSmtpPort
            // 
            this.tbSmtpPort.Location = new System.Drawing.Point(6, 45);
            this.tbSmtpPort.Name = "tbSmtpPort";
            this.tbSmtpPort.Size = new System.Drawing.Size(123, 20);
            this.tbSmtpPort.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.OpenDesigns);
            this.groupBox2.Controls.Add(this.OpenDossiers);
            this.groupBox2.Location = new System.Drawing.Point(12, 112);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(208, 79);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Bestanden";
            // 
            // OpenDossiers
            // 
            this.OpenDossiers.Location = new System.Drawing.Point(7, 20);
            this.OpenDossiers.Name = "OpenDossiers";
            this.OpenDossiers.Size = new System.Drawing.Size(195, 23);
            this.OpenDossiers.TabIndex = 0;
            this.OpenDossiers.Text = "Open Dossiers Folder";
            this.OpenDossiers.UseVisualStyleBackColor = true;
            this.OpenDossiers.Click += new System.EventHandler(this.OpenDossiers_Click);
            // 
            // OpenDesigns
            // 
            this.OpenDesigns.Location = new System.Drawing.Point(7, 49);
            this.OpenDesigns.Name = "OpenDesigns";
            this.OpenDesigns.Size = new System.Drawing.Size(195, 23);
            this.OpenDesigns.TabIndex = 0;
            this.OpenDesigns.Text = "Open Designs Folder";
            this.OpenDesigns.UseVisualStyleBackColor = true;
            this.OpenDesigns.Click += new System.EventHandler(this.OpenDesigns_Click);
            // 
            // SmoothboardMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 355);
            this.Controls.Add(this.tbcMain);
            this.MinimumSize = new System.Drawing.Size(636, 386);
            this.Name = "SmoothboardMainForm";
            this.Text = "Smoothboard";
            this.Load += new System.EventHandler(this.SmoothboardMainForm_Load);
            this.tpNewDossier.ResumeLayout(false);
            this.gbComment.ResumeLayout(false);
            this.gbComment.PerformLayout();
            this.gbDesign.ResumeLayout(false);
            this.gbDesign.PerformLayout();
            this.gbCustomer.ResumeLayout(false);
            this.gbCustomer.PerformLayout();
            this.tpDossiers.ResumeLayout(false);
            this.gbInfo.ResumeLayout(false);
            this.gbInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPreview)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tbcMain.ResumeLayout(false);
            this.tpSettings.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tpNewDossier;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbComment;
        private System.Windows.Forms.TextBox tbComment;
        private System.Windows.Forms.GroupBox gbDesign;
        private System.Windows.Forms.Label lbCustomDesign;
        private System.Windows.Forms.Button btnGenerateDesign;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox gbCustomer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbSize;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbTake;
        private System.Windows.Forms.TextBox tbBrought;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tpDossiers;
        private System.Windows.Forms.GroupBox gbInfo;
        private System.Windows.Forms.PictureBox pbPreview;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.Label lbTake;
        private System.Windows.Forms.Label lbBrought;
        private System.Windows.Forms.Label lbNumber;
        private System.Windows.Forms.Label lbSize;
        private System.Windows.Forms.Label lbAddress;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbDossiers;
        private System.Windows.Forms.TabControl tbcMain;
        private System.Windows.Forms.Button btnMail;
        private System.Windows.Forms.TextBox tbDocComment;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tpSettings;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tbSmtpHost;
        private System.Windows.Forms.TextBox tbSmtpPort;
        private System.Windows.Forms.Button tbUpdateSettings;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button OpenDesigns;
        private System.Windows.Forms.Button OpenDossiers;

    }
}

