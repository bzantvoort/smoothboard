﻿using System.Drawing;

namespace Smoothboard.Resources {
    public class Dossier {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public string Size { get; set; }
        public string BroughtAt { get; set; }
        public string TakeAt { get; set; }
        public string Comment { get; set; }
        public string Design { get; set; }
    }
}
